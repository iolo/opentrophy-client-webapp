module.exports = function (grunt) {
  'use strict';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        devel: true,
        browser: true,
        '-W030': true,//Expected an assignment or function call and instead saw an expression.
        '-W097': true,//Use the function form of "use strict".
        globals: {
          $: true,
          angular: true
        }
      },
      all: ['app/js/**/*.js']
    },
    concat: {
      options: {
        separator: ';'
      },
      all: {
        src: ['app/js/**/*.js'],
        dest: 'build/app/js/all.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%=pkg.name%> <%=pkg.version%> (build at ' + (new Date()) + ') */\n'
      },
      all: {
        files: {
          'build/app/js/all.min.js': ['<%=concat.all.dest%>']
        }
      }
    },
    jade: {
      options: {
        pretty: true,
        compileDebug: true
      },
      all: {
        expand: true,
        cwd: "app/",
        src: ["**/*.jade"],
        dest: "build/app",
        ext: ".html"
      }
    },
    copy: {
      all: {
        expand: true,
        cwd: 'app/',
        src: ['**', '!**/*.jade'],
        dest: 'build/app'
      }
    },
    watch: {
      options: {nospawn: true},
      all: {
        files: ['app/**/*', '!app/**/*.jade'],
        tasks: ['copy']
      },
      jade: {
        files: ['app/**/*.jade'],
        tasks: ['jade']
      }
    },
    clean: {
      build: ['build']
    },
    connect: {
      server: {
        options: {
          base: 'app',
          port: 8001,
          keepalive: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('rsync', function () {
    var done = this.async();
    var rsync_cmdline = 'rsync -az --delete ./build/app/ root@iolo.kr:/var/www/x/xclient';
    grunt.log.ok(rsync_cmdline);
    require('child_process').exec(rsync_cmdline, function (err, stdout, stderr) {
      if (err) {
        grunt.verbose.writeln(stderr);
        grunt.fail.fatal('rsync failed:' + err);
      } else {
        grunt.verbose.writeln(stdout);
        grunt.log.ok('rsync complete.');
      }
      done();
    });
  });
  grunt.registerTask('doxx', function () {
    var done = this.async();
    // XXX: doxx 0.6.0 contains broken template.
    var doxx_cmdline = './node_modules/.bin/doxx --template ./scripts/doxx.jade --source app/js --target build/docs/dox';
    require('child_process').exec(doxx_cmdline, function (err, stdout, stderr) {
      if (err) {
        grunt.verbose.writeln(stderr);
        grunt.fail.fatal('doxx failed:' + err);
      } else {
        grunt.verbose.writeln(stdout);
        grunt.log.ok('doxx complete.');
      }
      done(err);
    });
  });
  grunt.registerTask('default', ['build']);
  grunt.registerTask('test', ['jshint']);
  grunt.registerTask('docs', ['doxx']);
  grunt.registerTask('build', ['jshint', 'concat', 'uglify', 'jade', 'copy']);
  grunt.registerTask('deploy', ['build', 'rsync']);
  grunt.registerTask('server', ['connect']);

  grunt.event.on('watch', function (action, filepath, target) {
    if (grunt.file.isMatch(grunt.config('watch.all.files'), filepath)) {
      var copySrc = filepath.replace(grunt.config('copy.all.cwd'), '');
      grunt.config('copy.all.src', [copySrc]);
    }
    if (grunt.file.isMatch(grunt.config('watch.jade.files'), filepath)) {
      var jadeSrc = filepath.replace(grunt.config('jade.all.cwd'), '');
      grunt.config('jade.all.src', [jadeSrc]);
    }
  });
};
