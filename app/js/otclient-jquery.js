/*!
 * OpenTrophy Client Library - jQuery Version
 * Copyright (c) 2013 IoloTheBard <iolothebard@gmail.com>
 * All Rights Reserved.
 */
(function (window, console, $) {
  'use strict';

  $.otclient = (function () {

    //
    // compatibility
    //

    var DEBUG = true;//!!window.OPENTROPHY_DEBUG;

    function debug() {
      DEBUG && console.log.apply(console, arguments);
    }

    function _clone(obj) {
      return $.extend({}, obj);
    }

    function _extend(target, obj) {
      return $.extend(target, obj);
    }

    function _notify(event, param) {
      $(window).trigger(event, param);
    }

    //
    //
    //

    var config = {
      key: '',
      secret: '',
      connectUrl: 'http://api.opentrophy.com/auth/bearer',
      apiUrl: 'http://api.opentrophy.com/api/v1',
      dashboardUrl: 'http://www.opentrophy.com/player/',
      dashboardPopupSpec: 'width=340,height=490,menubar=no,status=no,directories=no,resizable=no'
    };

    var bearer = null;
    var currentApp = null;
    var currentPlayer = null;

    // custom events
    var CONNECT = 'ot-connect', DISCONNECT = 'ot-disconnect', SIGNIN = 'ot-signin', SIGNOUT = 'ot-signout';

    // http methods
    var GET = 'GET', POST = 'POST', PUT = 'PUT', DELETE = 'DELETE';

    /**
     * send api request with authorization headers.
     *
     * @param {string} method request method
     * @param {string} [path] relative path for api url or `null` for connect url.
     * @param {object} [data] request body
     * @param {object} [params] request uri params
     * @param {boolean} [noConnect=false] (re)connect on demand
     * @returns {Promise}
     * @private
     */
    function _request(method, path, data, params, noConnect) {
      if (path && !bearer) {
        if (noConnect) {
          return $.Deferred().reject({status: 403, message: 'forbidden by client'});
        }
        var connectInfo = {
          grant_type: 'password',
          key: config.key,
          secret: config.secret
        };
        return _request(POST, null, connectInfo)
          .then(function (connectData) {
            DEBUG && debug('auto connect ok:', arguments);

            bearer = _clone(connectData);
            currentApp = bearer.app;
            _notify(CONNECT, currentApp);

            // already signin?
            if (bearer.player) {
              currentPlayer = bearer.player;
              _notify(SIGNIN, currentPlayer);
            }

            // XXX: looks dangerous but don't worry! the last 'true' will break recursion.
            // retry the original request after auth
            return _request(method, path, data, params, true);
          });
      }
      var req = {
        type: method,
        url: path ? config.apiUrl + path : config.connectUrl,
        dataType: 'json',
        headers: {Accept: 'application/json'},
        xhrFields: {withCredentials: true}
      };
      if (data) {
        req.headers['Content-Type'] = 'application/json;charset=utf8';
        req.data = JSON.stringify(data);
      }
      if (params) {
        req.url += (req.url.indexOf('?') < 0 ? '?' : '&') + $.param(params);
      }
      if (bearer) {
        req.headers.Authorization = 'Bearer ' + bearer.access_token;
      }
      return $.ajax(req).then(
        function (data, status, xhr) {
          DEBUG && debug('***request ', req.type, req.url, 'ok:', status, data, xhr);
          return data;
        },
        function (xhr, status, err) {
          DEBUG && debug('***request ', req.type, req.url, 'err:', status, err, xhr);
          if (status === 0) {
            _notify(DISCONNECT);
          }
          var error;
          try {
            error = JSON.parse('(' + xhr.responseText + ')').error;
          } catch (e) {
            error = {status: status, message: xhr.responseText, cause: err};
          }
          return $.Deferred().reject(error);
        });
    }

    /**
     * create api wrapper for low-level RESTful resources.
     *
     * @param {string} plural resource name for list and url
     * @param {string} singular resource name for load/create/update/destroy
     * @returns {{list: Function, load: Function, create: Function, update: Function, destroy: Function}}
     * @private
     */
    function _resource(plural, singular) {
      var path = '/' + plural;
      return {
        list: function (params) {
          return _request(GET, path, null, params)
            .then(function (data) {
              DEBUG && debug('list ' + plural + ':', data);
              return data[plural];
            });
        },
        load: function (id) {
          return _request(GET, path + '/' + id)
            .then(function (data) {
              DEBUG && debug('load ' + singular + ' by id:', data);
              return data[singular];
            });
        },
        create: function (data) {
          DEBUG && debug('create ' + singular + ':', data);
          return _request(POST, path, data);
        },
        update: function (id, data) {
          DEBUG && debug('update ' + singular + ':', id, data);
          return _request(PUT, path + '/' + id, data);
        },
        destroy: function (id) {
          DEBUG && debug('destroy ' + singular + ':', id);
          return _request(DELETE, path + '/' + id);
        }
      };
    }

    //
    //
    //

    /**
     * create an OpenTrophy Client.
     *
     * @constructor
     */
    function OTClient() {
    }

    /**
     * configure OpenTrophy Client.
     *
     * `_config` contains properties:
     *
     *    - {string} key: API key (required)
     *    - {string} secret: API secret (required)
     *    - {string} connectUrl: 'http://api.opentrophy.com/auth/bearer'
     *    - {string} apiUrl: 'http://api.opentrophy.com/api/v1'
     *    - {string} dashboardUrl: 'http://www.opentrophy.com/player/'
     *
     * @param {object} _config
     */
    OTClient.prototype.configure = function (_config) {
      _extend(config, _config);
      DEBUG && debug('OTClient config:', config);
    };

    /**
     * get currently signed-in player or null.
     *
     * @returns {object}
     */
    OTClient.prototype.getCurrentPlayer = function () {
      return currentPlayer;
    };

    /**
     * get currently connected app of null.
     *
     * @returns {object}
     */
    OTClient.prototype.getCurrentApp = function () {
      return currentApp;
    };

    //
    // connect
    //

    /**
     * connect.
     *
     * @returns {Promise} current app(with optional current player)
     */
    OTClient.prototype.connect = function () {
      var connectInfo = {
        grant_type: 'password',
        key: config.key,
        secret: config.secret
      };
      return _request(POST, null, connectInfo)
        .then(function (data) {
          DEBUG && debug('connect ok:', arguments);

          bearer = _clone(data);
          currentApp = bearer.app;
          _notify(CONNECT, currentApp);

          // already signin?
          if (bearer.player) {
            currentPlayer = bearer.player;
            _notify(SIGNIN, currentPlayer);
          }

          return currentApp;
        });
    };

    /**
     * disconnect.
     *
     * @returns {Promise}
     */
    OTClient.prototype.disconnect = function () {
      var promise = _request(DELETE);
      bearer = null;
      currentApp = null;
      currentPlayer = null;
      _notify(SIGNOUT);
      _notify(DISCONNECT);
      return promise;
    };

    /**
     * reconnect.
     *
     * @param {function(err, result, count, max)} callback
     */
    OTClient.prototype.reconnect = function (callback) {
      if (this._reconnect_running) {
        DEBUG && debug('reconnect running!');
        return;
      }
      this._reconnect_running = true;

      var self = this;
      var count = 0;
      var max = 5;
      var interval = 500;

      var tryConnect = function () {
        count += 1;
        callback && callback(null, null, count, max);

        self.connect().then(function (res) {
          DEBUG && debug('reconnect#' + count + ' ok', res);
          delete self._reconnect_running;
          callback && callback(null, res);
        }, function (err) {
          DEBUG && debug('reconnect#' + count + ' fail:', err);
          if (err && err.status !== 0) {
            delete self._reconnect_running;
            callback && callback(err);
          }
          if (count < max) {
            // reconnect with longer interval
            interval *= 2;
            setTimeout(tryConnect, interval);
          } else {
            DEBUG && debug('reconnect giveup!');
            delete self._reconnect_running;
            callback && callback(err);
          }
        });
      };

      setTimeout(tryConnect, interval);
    };

    /**
     * ping.
     *
     * **test purpose only!**
     *
     * @returns {Promise} pong
     */
    OTClient.prototype.ping = function () {
      return _request(GET);
    };

    //
    // auth
    //

    /**
     * sign-up(aka. create user account).
     *
     * @param {string} provider "local" or oauth provider("facebook", "twitter", "google", ...)
     * @param {string} [username]
     * @param {string} [password]
     * @param {object} [profile] optional fields for email, icon, firstName, lastName, url, location, and so on.
     * @returns {Promise} current user
     */
    OTClient.prototype.signup = function (provider, username, password, profile) {
      var data = {
        provider: provider,
        username: username,
        password: password,
        profile: profile
      };
      return _request(POST, '/auth/signup', data)
        .then(function (data) {
          DEBUG && debug('signup ok:', data);
          return data;
        });
    };

    /**
     * sign-down(aka. destroy user account).
     *
     * @param {string} password
     * @returns {Promise}
     */
    OTClient.prototype.signdown = function (password) {
      var data = {password: password};
      return _request(DELETE, '/auth/signdown', data)
        .then(function (data) {
          DEBUG && debug('signdown ok:', data);
          currentPlayer = null;
          _notify(SIGNOUT);
        });
    };

    /**
     * sign-in(aka. login).
     *
     * @param {string} provider "local" or oauth provider("facebook", "twitter", "google", ...)
     * @param {string} username
     * @param {string} password
     * @param {boolean} [rememberMe=false]
     * @returns {Promise} current player
     */
    OTClient.prototype.signin = function (provider, username, password, rememberMe) {
      var data = {
        provider: provider,
        username: username,
        password: password,
        rememberMe: rememberMe
      };
      return _request(POST, '/auth/signin', data)
        .then(function (data) {
          DEBUG && debug('signin ok:', data);
          currentPlayer = _clone(data.player);
          _notify(SIGNIN, currentPlayer);
          return currentPlayer;
        });
    };

    /**
     * sign-out(aka. logout).
     *
     * @returns {Promise}
     */
    OTClient.prototype.signout = function () {
      var promise = _request(DELETE, '/auth/signout');
      currentPlayer = null;
      _notify(SIGNOUT);
      return promise;
    };

    /**
     * find user account using the `email`.
     *
     * @param {string} email
     * @returns {Promise}
     */
    OTClient.prototype.recoverAccountByEmail = function (email) {
      return _request(POST, '/auth/recover', {email: email});
    };

    /**
     * request email to activate account.
     *
     * @param {string} email
     * @returns {Promise}
     */
    OTClient.prototype.requestActivateAccountByEmail = function (email) {
      return _request(POST, '/auth/activate', {email: email});
    };

    /**
     * activate account by `token`.
     *
     * might be called back from email.
     *
     * @param {string} token
     * @returns {Promise}
     */
    OTClient.prototype.activateAccountByToken = function (token) {
      return _request(PUT, '/auth/activate', {token: token});
    };

    /**
     * request email to reset password.
     *
     * @param {string} email
     * @returns {Promise}
     */
    OTClient.prototype.requestResetPasswordByEmail = function (email) {
      return _request(POST, '/auth/password', {email: email});
    };

    /**
     * reset password by `token`.
     *
     * might be called back from email.
     *
     * @param {string} token
     * @param {string} password
     * @returns {Promise}
     */
    OTClient.prototype.resetPasswordByToken = function (token, password) {
      return _request(PUT, '/auth/password', {token: token, password: password});
    };

    //
    // common
    //

    /**
     * echo.
     *
     * **test purpose only!**
     *
     * @param {string} [method='GET']
     * @param {object} [data]
     * @param {object} [params]
     * @returns {Promise}
     */
    OTClient.prototype.echo = function (method, data, params) {
      return _request(method || GET, '/common/echo', data, params);
    };

    /**
     * TODO: find something...
     *
     * @param {string} q
     * @param {string} [type='all']
     * @param {number} [skip=0]
     * @param {number} [limit=0]
     * @returns {Promise}
     */
    OTClient.prototype.search = function (q, type, skip, limit) {
      return _request(GET, '/common/search', null, {q: q, type: type, skip: skip, limit: limit}).then(function (data) {
        return data.result;
      });
    };

    //
    // low-level RESTful resources
    //

    OTClient.prototype.achievements = _resource('achievements', 'achievement');
    OTClient.prototype.activities = _resource('activities', 'activity');
    OTClient.prototype.apps = _resource('apps', 'app');
    OTClient.prototype.comments = _resource('comments', 'comment');
    OTClient.prototype.deals = _resource('deals', 'deal');
    OTClient.prototype.devices = _resource('devices', 'device');
    OTClient.prototype.groups = _resource('groups', 'group');
    OTClient.prototype.items = _resource('items', 'item');
    OTClient.prototype.leaderboards = _resource('leaderboards', 'leaderboard');
    OTClient.prototype.players = _resource('players', 'player');
    OTClient.prototype.posts = _resource('posts', 'post');
    OTClient.prototype.products = _resource('products', 'product');
    OTClient.prototype.progresses = _resource('progresses', 'progress');
    OTClient.prototype.scores = _resource('scores', 'scores');
    OTClient.prototype.tags = _resource('tags', 'tags');
    OTClient.prototype.tickets = _resource('tickets', 'tickets');
    OTClient.prototype.users = _resource('users', 'user');

    // alias for current user, app and player
    var ME = 'me';

    function _me(id) {
      return id || ME;
    }

    //
    // users
    //

    /**
     * set password of the "current" user.
     *
     * @param {string} password
     * @param {string} passwordNew
     * @returns {Promise}
     */
    OTClient.prototype.setUserPassword = function (password, passwordNew) {
      return _request(POST, '/users/me/password', {password: password, passwordNew: passwordNew});
    };

    /**
     * set profile of the "current" user.
     *
     * @param {object} profile
     * @returns {Promise}
     */
    OTClient.prototype.setUserProfile = function (profile) {
      return _request(PUT, '/users/me/profile', profile);
    };

    /**
     * get profile by user id.
     *
     * @param {string} [userId='me']
     * @returns {Promise} user profile
     */
    OTClient.prototype.getUserProfile = function (userId) {
      return _request(GET, '/users/' + _me(userId) + '/profile').then(function (data) {
        return data.user;
      });
    };

    /**
     * get user details by user id.
     *
     * @param {string} [userId='me']
     * @returns {Promise} user details
     */
    OTClient.prototype.getUserById = function (userId) {
      return this.users.load(_me(userId));
    };

    /**
     * get user details by player id.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} user details
     */
    OTClient.prototype.getUserByPlayer = function (playerId) {
      var path = '/players/' + (_me(playerId)) + '/user';
      return _request(GET, path).then(function (data) {
        return data.user;
      });
    };

    /**
     * TODO: get user summary by user id.
     *
     * @param {string} [userId='me']
     * @returns {Promise} user summary
     */
    OTClient.prototype.getUserSummary = function (userId) {
      var path = '/users/' + _me(userId) + '/summary';
      return _request(GET, path);
    };

    /**
     * TODO: get tagged users.
     *
     * @param {string} tag
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} apps
     */
    OTClient.prototype.getTaggedUsers = function (tag, skip, limit) {
      var path = '/users/tagged/' + encodeURIComponent(tag);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.users;
      });
    };

    /**
     * TODO: report abuse of the other user.
     *
     * @param {string} userId
     * @param {string} [reason]
     * @param {string} [description]
     * @returns {Promise}
     */
    OTClient.prototype.reportAbuseUser = function (userId, reason, description) {
      var path = '/users/' + userId + '/abuse';
      var data = {reason: reason, description: description};
      return _request(POST, path, data);
    };

    /**
     * TODO: find matching users.
     *
     * @param {string} q
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.findUsers = function (q, skip, limit) {
      return this.search(q, 'user', skip, limit).then(function (data) {
        return data.users;
      });
    };

    //
    // apps
    //

    /**
     * get app details by app id.
     *
     * @param {string} [appId='me']
     * @returns {Promise} app details
     */
    OTClient.prototype.getAppById = function (appId) {
      return this.apps.load(_me(appId));
    };

    /**
     * get app details of the player.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} app details
     */
    OTClient.prototype.getPlayerApp = function (playerId) {
      var path = '/players/' + _me(playerId) + '/app';
      return _request(GET, path).then(function (data) {
        return data.app;
      });
    };

    /**
     * get apps of the user.
     * @param {string} [userId='me']
     * @returns {Promise} apps
     */
    OTClient.prototype.getUserApps = function (userId) {
      var path = '/users/' + _me(userId) + '/apps';
      return _request(GET, path).then(function (data) {
        return data.apps;
      });
    };

    /**
     * get all apps played by friends of the user.
     *
     * @param {string} userId
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise}
     */
    OTClient.prototype.getFriendsApps = function (userId, skip, limit) {
      var path = '/users/' + _me(userId) + '/apps/friends';
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.apps;
      });
    };

    /**
     * get app summary.
     *
     * @param {string} [appId='me']
     * @returns {Promise} app summary(with with leaderboards,achievements,...)
     */
    OTClient.prototype.getAppSummary = function (appId) {
      var path = '/apps/' + _me(appId) + '/summary';
      return _request(GET, path);
    };

    /**
     * get tagged apps.
     *
     * @param {string} tag
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} apps
     */
    OTClient.prototype.getTaggedApps = function (tag, skip, limit) {
      var path = '/apps/tagged/' + encodeURIComponent(tag);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.apps;
      });
    };

    /**
     * get app secret.
     * TODO: make secure
     *
     * @param {string} appId
     * @returns {Promise} secret string
     */
    OTClient.prototype.getAppSecret = function (appId) {
      return _request(GET, '/apps/' + appId + '/secret').then(function (data) {
        return data.secret;
      });
    };

    /**
     * reset app secret.
     * TODO: make secure
     *
     * @param {string} appId
     * @returns {Promise} success or not
     */
    OTClient.prototype.resetAppSecret = function (appId, secret) {
      var path = '/apps/' + appId + '/secret';
      var data = {secret: secret};
      return _request(PUT, path, data);
    };

    /**
     * TODO: report abuse of the app.
     *
     * @param {string} appId
     * @param {string} [reason]
     * @param {string} [description]
     * @returns {Promise}
     */
    OTClient.prototype.reportAbuseApp = function (appId, reason, description) {
      var path = '/apps/' + appId + '/abuse';
      var data = {reason: reason, description: description};
      return _request(POST, path, data);
    };

    /**
     * TODO: find matching apps.
     *
     * @param {string} q
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} apps
     */
    OTClient.prototype.findApps = function (q, skip, limit) {
      return this.search(q, 'app', skip, limit).then(function (data) {
        return data.apps;
      });
    };

    //
    // players
    //

    /**
     * set profile of the "current" player.
     *
     * @param {object} profile
     * @returns {Promise}
     */
    OTClient.prototype.setPlayerProfile = function (profile) {
      return _request(PUT, '/players/me/profile', profile);
    };

    /**
     * get profile by player id.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} player profile
     */
    OTClient.prototype.getPlayerProfile = function (playerId) {
      return _request(GET, '/players/' + _me(playerId) + '/profile').then(function (data) {
        return data.player;
      });
    };

    /**
     * get player details by player id.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} player details
     */
    OTClient.prototype.getPlayerById = function (playerId) {
      return this.players.load(_me(playerId));
    };

    /**
     * get players of the user.
     *
     * @param {string] [userId='me']
     * @returns {Promise} players
     */
    OTClient.prototype.getUserPlayers = function (userId) {
      var path = '/users/' + _me(userId) + '/players';
      return _request(GET, path).then(function (data) {
        return data.players;
      });
    };

    /**
     * get summary of the player.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} player summary
     */
    OTClient.prototype.getPlayerSummary = function (playerId) {
      var path = '/players/' + _me(playerId) + '/summary';
      return _request(GET, path);
    };

    /**
     * TODO: get tagged players.
     *
     * @param {string} tag
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} apps
     */
    OTClient.prototype.getTaggedPlayers = function (tag, skip, limit) {
      var path = '/players/tagged/' + encodeURIComponent(tag);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.players;
      });
    };

    /**
     * TODO: report abuse of the other player.
     *
     * @param {string} playerId
     * @param {string} [reason]
     * @param {string} [description]
     * @returns {Promise}
     */
    OTClient.prototype.reportAbusePlayer = function (playerId, reason, description) {
      var path = '/players/' + playerId + '/abuse';
      var data = {reason: reason, description: description};
      return _request(POST, path, data);
    };

    /**
     * TODO: find matching players.
     *
     * @param {string} q
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} players
     */
    OTClient.prototype.findPlayers = function (q, skip, limit) {
      return this.search(q, 'player', skip, limit).then(function (data) {
        return data.apps;
      });
    };

    //
    // leaderboards/scores
    //

    /**
     * get leaderboard by id.
     *
     * @param {string} leaderboardId
     * @returns {Promise} leaderboard
     */
    OTClient.prototype.getLeaderboardById = function (leaderboardId) {
      return this.leaderboards.load(leaderboardId);
    };

    /**
     * get all leaderboards of the app.
     *
     * @param {string} [appId='me']
     * @returns {Promise} leaderboards
     */
    OTClient.prototype.getAppLeaderboards = function (appId) {
      var path = '/apps/' + _me(appId) + '/leaderboards';
      return _request(GET, path).then(function (data) {
        return data.leaderboards;
      });
    };

    /**
     * get the default leaderboard of the app.
     *
     * @param {string} [appId='me']
     * @returns {Promise} leaderboard
     */
    OTClient.prototype.getAppLeaderboard = function (appId) {
      var path = '/apps/' + _me(appId) + '/leaderboards/me';
      return _request(GET, path).then(function (data) {
        return data.leaderboard;
      });
    };

    /**
     * get leaderboard and scores.
     *
     * @param {string} [leaderboardId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} leaderboard and scores
     */
    OTClient.prototype.getLeaderboardScores = function (leaderboardId, skip, limit) {
      var path = '/leaderboards/' + _me(leaderboardId) + '/scores';
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {leaderboard: data.leaderboard, scores: data.scores};
      });
    };

    /**
     * get leaderboard and scores reported by the player.
     *
     * @param {string} [leaderboardId='me']
     * @param {string} [playerId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} leaderboard and scores
     */
    OTClient.prototype.getLeaderboardScoresByPlayer = function (leaderboardId, playerId, skip, limit) {
      var path = '/leaderboards/' + _me(leaderboardId) + '/scores/players/' + _me(playerId);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {leaderboard: data.leaderboard, scores: data.scores};
      });
    };

    /**
     * get leaderboard and scores reported by friends of the user.
     *
     * @param {string} [leaderboardId='me']
     * @param {string} [userId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} leaderboard and scores
     */
    OTClient.prototype.getLeaderboardScoresByFriends = function (leaderboardId, userId, skip, limit) {
      var path = '/leaderboards/' + _me(leaderboardId) + '/scores/friends/' + _me(userId);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {leaderboard: data.leaderboard, scores: data.scores};
      });
    };

    /**
     * get leaderboard and scores reported by near players.
     *
     * @param {string} [leaderboardId='me']
     * @param {number} lat
     * @param {number} lng
     * @param {number} radius
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} leaderboard and scores
     */
    OTClient.prototype.getLeaderboardScoresByNear = function (leaderboardId, lat, lng, radius, skip, limit) {
      var path = '/leaderboards/' + _me(leaderboardId) + '/scores/near/' + [lat, lng, radius].join(',');
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {leaderboard: data.leaderboard, scores: data.scores};
      });
    };

    /**
     * get leaderboard summary.
     *
     * @param {string} [leaderboardId]
     * @returns {Promise} leaderboard summary
     */
    OTClient.prototype.getLeaderboardSummary = function (leaderboardId) {
      var path = '/leaderboards/' + leaderboardId + '/summary';
      return _request(GET, path);
    };

    /**
     * report a score entry.
     *
     * @param {string} leaderboardId
     * @param {number} value
     * @returns {Promise}
     */
    OTClient.prototype.reportScore = function (leaderboardId, value) {
      var path = '/leaderboards/' + leaderboardId + '/scores';
      var data = {value: value};
      return _request(POST, path, data);
    };

    /**
     * report multiple score entries at once.
     *
     * @param {array.<object>} scores - array of leaderboardId and value
     * @returns {Promise}
     */
    OTClient.prototype.reportScores = function (scores) {
      var path = '/leaderboards/scores';
      var data = {scores: scores};
      return _request(POST, path, data);
    };

    //
    // achievements/progresses
    //

    /**
     * get achievement by id.
     *
     * @param {string} achievementId
     * @returns {Promise} achievement
     */
    OTClient.prototype.getAchievementById = function (achievementId) {
      return this.achievements.load(achievementId);
    };

    /**
     * get all achievements of the app.
     *
     * @param {string} [appId='me']
     * @returns {Promise} achievements
     */
    OTClient.prototype.getAppAchievements = function (appId) {
      var path = '/apps/' + _me(appId) + '/achievements';
      return _request(GET, path).then(function (data) {
        return data.achievements;
      });
    };

    /**
     * get all achievements with the best "progress" of the player.
     *
     * @param {string} [playerId='me']
     * @returns {Promise} achievements with progress
     */
    OTClient.prototype.getPlayerAchievements = function (playerId) {
      var path = '/players/' + _me(playerId) + '/achievements';
      return _request(GET, path).then(function (data) {
        return data.achievements;
      });
    };

    /**
     *  get the achievement and progresses.
     *
     * @param {string} achievementId
     * @param {number} [progress]
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} achievement and progresses
     */
    OTClient.prototype.getAchievementProgresses = function (achievementId, progress, skip, limit) {
      var path = '/achievements/' + achievementId + '/progresses';
      var params = {progress: progress, skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {achievement: data.achievement, progresses: data.progresses};
      });
    };

    /**
     *  get achievement and progresses reported by the player
     *
     * @param {string} achievementId
     * @param {number} [progress]
     * @param {string} [playerId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} achievement and progresses
     */
    OTClient.prototype.getAchievementProgressesByPlayer = function (achievementId, progress, playerId, skip, limit) {
      var path = '/achievements/' + achievementId + '/progresses/player/' + _me(playerId);
      var params = {progress: progress, skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {achievement: data.achievement, progresses: data.progresses};
      });
    };

    /**
     *  get achievement and progresses reported by friends of the user.
     *
     * @param {string} achievementId
     * @param {number} [progress]
     * @param {string} [userId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} achievement and progresses
     */
    OTClient.prototype.getAchievementProgressesByFriends = function (achievementId, progress, userId, skip, limit) {
      var path = '/achievements/' + achievementId + '/progresses/friends/' + _me(userId);
      var params = {progress: progress, skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return {achievement: data.achievement, progresses: data.progresses};
      });
    };

    /**
     * get achievement summary.
     *
     * @param {string} achievementId
     * @returns {Promise} achievement summary
     */
    OTClient.prototype.getAchievementSummary = function (achievementId) {
      var path = '/achievements/' + achievementId + '/summary';
      return _request(GET, path).then(function (data) {
        return {achievement: data.achievement, progress: data.progress, progresses: data.progresses};
      });
    };

    /**
     * create a progress entry.
     *
     * @param {string} achievementId
     * @param {number} value
     * @returns {Promise}
     */
    OTClient.prototype.reportProgress = function (achievementId, value) {
      var path = '/achievements/' + achievementId + '/progresses';
      var data = {value: value};
      return _request(POST, path, data);
    };

    /**
     * create multiple progress entries at once.
     *
     * @param {array.<object>} progresses array of achievementId and value
     * @returns {Promise}
     */
    OTClient.prototype.reportProgresses = function (progresses) {
      var path = '/achievements/progresses';
      var data = {progresses: progresses};
      return _request(POST, path, data);
    };

    //
    // relations/friends
    //

    // predefined relations
    var FRIENDS = 'friends', INCOMING = 'incoming', OUTGOING = 'outgoing', BLOCKED = 'blocked', SUGGESTED = 'suggested';

    /**
     * get all relation groups of the current user.
     *
     * @returns {Promise} relation groups
     * @private
     */
    OTClient.prototype._getRelationGroups = function () {
      var path = '/users/me/relations';
      return _request(GET, path).then(function (data) {
        return data.groups;
      });
    };

    /**
     * get all members in the relation group of the current user.
     *
     * @param {string} rel
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} members
     * @private
     */
    OTClient.prototype._getRelationMembers = function (rel, skip, limit) {
      var path = '/users/me/relations/' + rel + '/members';
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.members;
      });
    };

    /**
     * get all members as users in the relation group of the current user.
     *
     * @param {string} rel
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     * @private
     */
    OTClient.prototype._getRelationUsers = function (rel, skip, limit) {
      var path = '/users/me/relations/' + rel + '/users';
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.users;
      });
    };

    /**
     * get all members as players who plays the app in the relation group of the current user.
     *
     * @param {string} rel
     * @param {string} [appId='me']
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} players
     * @private
     */
    OTClient.prototype._getRelationPlayers = function (rel, appId, skip, limit) {
      var path = '/users/me/relations/' + rel + '/players/' + _me(appId);
      var params = {skip: skip, limit: limit};
      return _request(GET, path, null, params).then(function (data) {
        return data.players;
      });
    };

    /**
     * make a relation.
     *
     * @param {string} relation
     * @param {string} anotherId
     * @param {object} [data] request body
     * @returns {Promise}
     * @private
     */
    OTClient.prototype._relate = function (relation, anotherId, data) {
      var path = '/users/me/relations/' + relation + '/users/' + anotherId;
      return _request(POST, path, data);
    };

    /**
     * break a relation.
     *
     * @param {string} relation
     * @param {string} anotherId
     * @returns {Promise}
     * @private
     */
    OTClient.prototype._unrelate = function (relation, anotherId) {
      var path = '/users/me/relations/' + relation + '/users/' + anotherId;
      return _request(DELETE, path);
    };

    /**
     * get users in `friend` relation group.
     *
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.getFriendsUsers = function (skip, limit) {
      return this._getRelationUsers(FRIENDS, skip, limit);
    };

    /**
     * get users in `incoming` relation group.
     *
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.getIncomingUsers = function (skip, limit) {
      return this._getRelationUsers(INCOMING, skip, limit);
    };

    /**
     * get users in `outgoing` relation group.
     *
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.getOutgoingUsers = function (skip, limit) {
      return this._getRelationUsers(OUTGOING, skip, limit);
    };

    /**
     * get users in `suggested` relation group.
     *
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.getSuggestedUsers = function (skip, limit) {
      return this._getRelationUsers(SUGGESTED, skip, limit);
    };

    /**
     * get users in `blocked` relation group.
     *
     * @param {number} [skip]
     * @param {number} [limit]
     * @returns {Promise} users
     */
    OTClient.prototype.getBlockedUsers = function (skip, limit) {
      return this._getRelationUsers(BLOCKED, skip, limit);
    };

    /**
     * send friend request to the another user.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.sendFriendRequest = function (anotherId) {
      return this._relate(OUTGOING, anotherId);
    };

    /**
     * cancel friend request to the another user.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.cancelFriendRequest = function (anotherId) {
      return this._unrelate(OUTGOING, anotherId);
    };

    /**
     * accept friend request from the another user.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.acceptFriendRequest = function (anotherId) {
      return this._relate(INCOMING, anotherId);
    };

    /**
     * refuse(silently ignore) friend request from the another user.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.refuseFriendRequest = function (anotherId) {
      return this._unrelate(INCOMING, anotherId);
    };

    /**
     * add the another user into `friends` relation group.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.friendUser = function (anotherId) {
      return this._relate(FRIENDS, anotherId);
    };

    /**
     * remove the another user from `friends` relation group.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.unfriendUser = function (anotherId) {
      return this._unrelate(FRIENDS, anotherId);
    };

    /**
     * add the another user into `blocked` relation group.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.blockUser = function (anotherId) {
      return this._relate(BLOCKED, anotherId);
    };

    /**
     * remove the another user from `blocked` relation group.
     *
     * @param {string} anotherId
     * @returns {Promise}
     */
    OTClient.prototype.unblockUser = function (anotherId) {
      return this._unrelate(BLOCKED, anotherId);
    };

    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------

    //
    // linking with popup window
    //

    // XXX: ie 8,9 doesn't support cross domain messaging with popup window
    /**
     * open the dashboard in popup window.
     *
     * @param {string} [path=''] relative path and/or query params
     * @returns {Window} window object
     */
    OTClient.prototype.openDashboard = function (path) {
      var url = config.dashboardUrl + (path || '');
      DEBUG && debug('show dashboard: url=' + url);

      // XXX: secure way to transfer session?
      //if (this.bearer) url += '?access_token=' + this.bearer.access_token;

      if (this.otDashboardWindow) {
        DEBUG && debug('reopen dashboard:', url);
        this.otDashboardWindow.location.href = url;
      } else {
        DEBUG && debug('open dashboard:', url);
        this.otDashboardWindow = window.open(url, 'otDashboardWindow', config.dashboardPopupSpec);
        var self = this;
        $(this.otDashboardWindow).unload(function (event) {
          DEBUG && debug('dashboard window unload', event);
          // XXX: transfer back session
          self.connect().done();
          delete self.otDashboardWindow;
        });
      }
      this.otDashboardWindow.focus();
      return this.otDashboardWindow;
    };

    /**
     * close the dashboard.
     */
    OTClient.prototype.closeDashboard = function () {
      // XXX: transfer back session
      this.connect().done();

      if (this.otDashboardWindow) {
        DEBUG && debug('close dashboard');
        this.otDashboardWindow.close();
        delete this.otDashboardWindow;
      }
    };

    /**
     * open leaderboards in popup window.
     *
     * @returns {Window} window object
     */
    OTClient.prototype.openLeaderboards = function () {
      return this.openDashboard('#/leaderboards');
    };

    /**
     *
     * open achievements in popup window.
     *
     * @returns {Window} window object
     */
    OTClient.prototype.openAchievements = function () {
      return this.openDashboard('#/achievements');
    };

    //
    // embedding with iframe
    //

    /**
     * show the dashboard in iframe.
     *
     * @param {string} [path=''] relative path and/or query params
     * @returns {Node} iframe element
     */
    OTClient.prototype.showDashboard = function (path) {
      var url = config.dashboardUrl + (path || '');
      DEBUG && debug('show dashboard: url=' + url);

      // XXX: secure way to transfer session?
      //if (this.bearer) url += '?access_token=' + this.bearer.access_token;

      if (this.otDashboardFrame) {
        DEBUG && debug('reshow dashboard');
        this.otDashboardFrame.attr('src', url);
      } else {
        DEBUG && debug('show dashboard');
        var backdrop = $('#otDashboardBackdrop');
        if (!backdrop.length) {
          DEBUG && debug('create dashboard backdrop');
          backdrop = $('<div/>', {id: 'otDashboardBackdrop'}).appendTo(document.body);
        }
        backdrop.show();
        var container = $('#otDashboardContainer');
        if (!container.length) {
          DEBUG && debug('create dashboard container');
          container = $('<div/>', {id: 'otDashboardContainer'}).appendTo(document.body);
        }
        container.show();
        var closer = $('#otDashboardCloser');
        if (!closer.length) {
          DEBUG && debug('create dashboard closer');
          closer = $('<div/>', {id: 'otDashboardCloser'}).appendTo(container).click(this.hideDashboard.bind(this));
        }
        closer.show();
        DEBUG && debug('create dashboard frame');
        this.otDashboardFrame = $('<iframe/>', {id: 'otDashboardFrame', src: url}).appendTo(container);
      }
      this.otDashboardFrame.focus();
      return this.otDashboardFrame;
    };

    /**
     * hide the dashboard.
     */
    OTClient.prototype.hideDashboard = function () {
      $('#otDashboardBackdrop').hide();
      $('#otDashboardContainer').hide();
      $('#otDashboardCloser').hide();
      if (this.otDashboardFrame) {
        DEBUG && debug('hide dashboard');

        // XXX: transfer back session
        this.connect().done();

        this.otDashboardFrame.remove();
        this.otDashboardFrame = null;
      }
    };

    /**
     * show leaderboards in iframe.
     *
     * @returns {Node} iframe element
     */
    OTClient.prototype.showLeaderboards = function () {
      return this.showDashboard('#/leaderboards');
    };

    /**
     * show the leaderboard in iframe.
     *
     * @returns {Node} iframe element
     */
    OTClient.prototype.showLeaderboard = function (leaderboardId) {
      return this.showDashboard('#/leaderboards/' + leaderboardId);
    };

    /**
     * show achievements in iframe.
     *
     * @returns {Node} iframe element
     */
    OTClient.prototype.showAchievements = function () {
      return this.showDashboard('#/achievements');
    };

    /**
     * show the achievement in iframe.
     *
     * @returns {Node} iframe element
     */
    OTClient.prototype.showAchievements = function (achievementId) {
      return this.showDashboard('#/achievements/' + achievementId);
    };

    return new OTClient();
  }());

}(window, console, jQuery));
